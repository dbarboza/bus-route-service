Bus Route Service 
===================

Simple service to solve the [bus-route-challange](https://github.com/goeuro/challenges/tree/master/bus_route_challenge).

## Requirements

The only requirement for building and running this app is `JDK 8`.

Nothing more is required for building/running this app using gradle.

## Building and running the app

This app was built using [Gradle](https://gradle.org/) as it build system and leverages the [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) 
to make sure the right version is being used.

* Use `./gradlew build` and then `java -jar build/libs/bus-route-service.jar --data.file=src/test/resources/sample-data.txt` 
to run the executable jar created by spring-boot with the sample data provided. Note that tests will run as part of the 
build as well.

After that you will be able access the api through the following URL:
`http://localhost:8088/api/direct?dep_sid=3&arr_sid=6`

## Assumptions

As it wasn't clear in the problem statement, the solution was implemented with the follow assumption in mind:

A Bus Route doesn't consider the **_order of the stations_**, which means the service will say there is a direct 
route for two stations independent if it's coming from the departure or the arrival.

Example: the bus route line `1 3 1 6 5` will return true either for `dep_sid=3&arr_sid=6` and `dep_sid=6&arr_sid=3`.

There's a test to cover this assumption in `BusRouteTest`.

