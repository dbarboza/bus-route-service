package com.goeuro.busrouteservice.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.goeuro.busrouteservice.domain.repository.BusRouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class BusRouteController {

    private BusRouteRepository repository;

    @Autowired
    public BusRouteController(BusRouteRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/api/direct", produces = APPLICATION_JSON_VALUE)
    public DirectBusRouteResponse direct(@RequestParam("dep_sid") int departureSid,
                                         @RequestParam("arr_sid") int arrivalSid) {
        boolean directBusRoute = repository.hasBusRouteConnecting(departureSid, arrivalSid);

        return new DirectBusRouteResponse(departureSid, arrivalSid, directBusRoute);
    }

    public static class DirectBusRouteResponse {
        @JsonProperty("dep_sid")
        private final int departureSid;
        @JsonProperty("arr_sid")
        private final int arrivalSid;
        @JsonProperty("direct_bus_route")
        private final boolean directBusRoute;

        public DirectBusRouteResponse(int departureSid, int arrivalSid, boolean directBusRoute) {
            this.departureSid = departureSid;
            this.arrivalSid = arrivalSid;
            this.directBusRoute = directBusRoute;
        }

        public int getDepartureSid() {
            return departureSid;
        }

        public int getArrivalSid() {
            return arrivalSid;
        }

        public boolean isDirectBusRoute() {
            return directBusRoute;
        }
    }
}
