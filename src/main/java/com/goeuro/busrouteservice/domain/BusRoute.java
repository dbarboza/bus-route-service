package com.goeuro.busrouteservice.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class BusRoute {

    private static final String AT_LEAST_THREE_INTEGERS_MESSAGE = "A Bus Route line should have at least 3 integers";
    private int routeId;
    private Set<Integer> stations;

    public BusRoute(String routeLine) {
        String[] busRouteIntegers = routeLine.split(" ");

        if (busRouteIntegers.length < 3) throw new IllegalArgumentException(AT_LEAST_THREE_INTEGERS_MESSAGE);

        routeId = Integer.parseInt(busRouteIntegers[0]);

        stations = Arrays.stream(busRouteIntegers)
                .skip(1) //the first one is the route id
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
    }

    public int getRouteId() {
        return routeId;
    }

    public Set<Integer> getStations() {
        return Collections.unmodifiableSet(stations);
    }

    public boolean connects(int departureStationId, int arrivalStationId) {
        return stations.contains(departureStationId) && stations.contains(arrivalStationId);
    }
}
