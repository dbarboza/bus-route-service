package com.goeuro.busrouteservice.domain.repository;

import com.goeuro.busrouteservice.domain.BusRoute;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BusRouteRepository {

    private final List<BusRoute> routes;

    public BusRouteRepository(@Value("${data.file}") String dataFile) throws IOException {
        this.routes = Files.lines(Paths.get(dataFile))
                .skip(1) //ignores first line
                .map(BusRoute::new)
                .collect(Collectors.toList());
    }

    public List<BusRoute> allRoutes() {
        return Collections.unmodifiableList(routes);
    }

    public boolean hasBusRouteConnecting(int departureSid, int arrivalSid) {
        return routes.stream().anyMatch(busRoute -> busRoute.connects(departureSid, arrivalSid));
    }
}
