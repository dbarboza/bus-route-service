package com.goeuro.busrouteservice.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class BusRouteTest {

    @Test
    public void shouldCreateBusRoute() throws Exception {
        BusRoute busRoute = new BusRoute("1 3 1 6 5");

        assertThat(busRoute.getRouteId()).isEqualTo(1);
        assertThat(busRoute.getStations())
                .hasSize(4)
                .containsExactlyInAnyOrder(3, 1, 6, 5);

        assertThat(busRoute.connects(3, 6))
                .as("Should connect 3 to 6")
                .isTrue();

        assertThat(busRoute.connects(6, 3))
                .as("Should connect 6 to 3")
                .isTrue();

    }

    @Test
    public void shouldNotCreateBusRouteifLessThanThreeNumbers() throws Exception {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new BusRoute("1 2"))
                .withMessage("A Bus Route line should have at least 3 integers");
    }

    @Test
    public void shouldNotAllowCreationOfBusRouteWithNonNumbers() throws Exception {
        assertThatExceptionOfType(NumberFormatException.class)
                .isThrownBy(() -> new BusRoute("1 2 A"))
                .withMessageContaining("string: \"A\"");

    }
}