package com.goeuro.busrouteservice.domain.repository;

import org.junit.Test;

import java.nio.file.NoSuchFileException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class BusRouteRepositoryTest {

    private static final String INEXISTENT_DATA_FILE = "INEXISTENT_DATA_FILE";

    @Test
    public void shouldThrowExceptionWhenDataFileDoesntExist() throws Exception {

        assertThatExceptionOfType(NoSuchFileException.class)
                .isThrownBy(() -> new BusRouteRepository(INEXISTENT_DATA_FILE))
                .withMessageContaining(INEXISTENT_DATA_FILE);
    }

    @Test
    public void shouldCreateRepositoryFromSampleFile() throws Exception {
        BusRouteRepository repository = new BusRouteRepository(getFilePath("/sample-data.txt"));

        assertThat(repository.allRoutes())
                .hasSize(3);
    }

    private String getFilePath(String resource) {
        return BusRouteRepository.class.getResource(resource).getFile();
    }
}