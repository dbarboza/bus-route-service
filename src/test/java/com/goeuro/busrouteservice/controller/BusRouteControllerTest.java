package com.goeuro.busrouteservice.controller;

import com.goeuro.busrouteservice.domain.repository.BusRouteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class BusRouteControllerTest {

    @Mock
    private BusRouteRepository repository;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        BusRouteController busRouteController = new BusRouteController(repository);
        mockMvc = MockMvcBuilders.standaloneSetup(busRouteController).build();
    }

    @Test
    public void shouldRespondToADirectGETRequest() throws Exception {

        when(repository.hasBusRouteConnecting(3, 6))
                .thenReturn(true);

        mockMvc.perform(get("/api/direct?dep_sid=3&arr_sid=6"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dep_sid", equalTo(3)))
                .andExpect(jsonPath("$.arr_sid", equalTo(6)))
                .andExpect(jsonPath("$.direct_bus_route", equalTo(true)))
        ;

    }
}